/**
 * @file
 * Apply circlify JS to all elements with class 'circliful'.
 */

(function($) {
  Drupal.behaviors.circliful = {
    attach: function(context, settings) {
      $('.circliful').once(function() {
        if ($.isFunction($.fn.waypoint)) {
          $(this).waypoint(function() {
            $(this).empty().circliful();
          },{offset: 'bottom-in-view'});
        }
        $(this).empty().circliful();
      });
    }
  };
})(jQuery);
